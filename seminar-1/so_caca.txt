so@c48ead2036d3:~$ ./commands_docker.sh 192.168.10.0,192.168.20.0,192.168.30.0,192.168.40.0

 NAS Parallel Benchmarks 3.3 -- EP Benchmark

 Number of random numbers generated:       536870912
 Number of active processes:                       4

EP Benchmark Results:

CPU Time =    6.2006
N = 2^   28
No. Gaussian Pairs =     210832767.
Sums =    -4.295875165634796D+03   -1.580732573678614D+04
Counts:
  0      98257395.
  1      93827014.
  2      17611549.
  3       1110028.
  4         26536.
  5           245.
  6             0.
  7             0.
  8             0.
  9             0.


 EP Benchmark Completed.
 Class           =                        A
 Size            =                536870912
 Iterations      =                        0
 Time in seconds =                     6.20
 Total processes =                        4
 Compiled procs  =                        4
 Mop/s total     =                    86.58
 Mop/s/process   =                    21.65
 Operation type  = Random numbers generated
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              09 Oct 2018

 Compile options:
    MPIF77       = mpifort
    FLINK        = $(MPIF77)
    FMPI_LIB     = -L/usr/local/lib -lmpi
    FMPI_INC     = -I/usr/local/include
    FFLAGS       = -O
    FLINKFLAGS   = -O
    RAND         = randi8


 Please send feedbacks and/or the results of this run to:

 NPB Development Team 
 Internet: npb@nas.nasa.gov




 NAS Parallel Benchmarks 3.3 -- CG Benchmark

 Size:      14000
 Iterations:    15
 Number of active processes:     4
 Number of nonzeroes per row:       11
 Eigenvalue shift: .200E+02

   iteration           ||r||                 zeta
        1       0.30634143529489E-12    19.9997581277040
        2       0.31096276403002E-14    17.1140495745506
        3       0.30804037245735E-14    17.1296668946143
        4       0.31368886171027E-14    17.1302113581193
        5       0.30931762620174E-14    17.1302338856353
        6       0.30711211120903E-14    17.1302349879482
        7       0.30014434726280E-14    17.1302350498916
        8       0.30091464390590E-14    17.1302350537510
        9       0.30845738922029E-14    17.1302350540101
       10       0.30464804270749E-14    17.1302350540284
       11       0.30356703468820E-14    17.1302350540298
       12       0.30110387739490E-14    17.1302350540299
       13       0.29937783924423E-14    17.1302350540299
       14       0.30298504149112E-14    17.1302350540299
       15       0.30223982636897E-14    17.1302350540299
 Benchmark completed 
 VERIFICATION SUCCESSFUL 
 Zeta is     0.1713023505403E+02
 Error is    0.5226337199892E-13


 CG Benchmark Completed.
 Class           =                        A
 Size            =                    14000
 Iterations      =                       15
 Time in seconds =                     0.91
 Total processes =                        4
 Compiled procs  =                        4
 Mop/s total     =                  1636.64
 Mop/s/process   =                   409.16
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              09 Oct 2018

 Compile options:
    MPIF77       = mpifort
    FLINK        = $(MPIF77)
    FMPI_LIB     = -L/usr/local/lib -lmpi
    FMPI_INC     = -I/usr/local/include
    FFLAGS       = -O
    FLINKFLAGS   = -O
    RAND         = randi8


 Please send feedbacks and/or the results of this run to:

 NPB Development Team 
 Internet: npb@nas.nasa.gov


[c48ead2036d3:00361] WARNING: local probe returned unhandled shell:unknown assuming bash


 NAS Parallel Benchmarks 3.3 -- BT Benchmark 

 No input file inputbt.data. Using compiled defaults
 Size:   64x  64x  64
 Iterations:  200    dt:   0.0008000
 Number of active processes:     4

 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Verification being performed for class A
 accuracy setting for epsilon =  0.1000000000000E-07
 Comparison of RMS-norms of residual
           1 0.1080634671464E+03 0.1080634671464E+03 0.7101254243319E-14
           2 0.1131973090122E+02 0.1131973090122E+02 0.9415542762816E-15
           3 0.2597435451158E+02 0.2597435451158E+02 0.6838887328685E-15
           4 0.2366562254468E+02 0.2366562254468E+02 0.6455215276928E-14
           5 0.2527896321175E+03 0.2527896321175E+03 0.1259241302557E-13
 Comparison of RMS-norms of solution error
           1 0.4234841604053E+01 0.4234841604053E+01 0.1048656009767E-14
           2 0.4439028249700E+00 0.4439028249700E+00 0.6752834174215E-14
           3 0.9669248013635E+00 0.9669248013635E+00 0.3100139909806E-14
           4 0.8830206303977E+00 0.8830206303977E+00 0.6286506715733E-15
           5 0.9737990177083E+01 0.9737990177083E+01 0.1824151397873E-14
 Verification Successful


 BT Benchmark Completed.
 Class           =                        A
 Size            =             64x  64x  64
 Iterations      =                      200
 Time in seconds =                    16.36
 Total processes =                        4
 Compiled procs  =                        4
 Mop/s total     =                 10286.22
 Mop/s/process   =                  2571.56
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              09 Oct 2018

 Compile options:
    MPIF77       = mpifort
    FLINK        = $(MPIF77)
    FMPI_LIB     = -L/usr/local/lib -lmpi
    FMPI_INC     = -I/usr/local/include
    FFLAGS       = -O
    FLINKFLAGS   = -O
    RAND         = (none)


 Please send feedbacks and/or the results of this run to:

 NPB Development Team 
 Internet: npb@nas.nasa.gov
